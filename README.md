# Bitcoin Wallet Server

Electrum Based Bitcoin Wallet Server

## Parent image

```
python:3.9.12-buster
```

## Dependencies

None

## Entrypoint & CMD

Since this Image is only used during CI we don't need an entrypoint.

## Functions

## Build Image

### Automated Build

Through the new build pipeline all of our Docker images are built via Gitlab CI/CD and pushed to the Gitlab Container Registry. You usually don't need to build the image on your own, just push your commits to the git. The Image will be tagged after your branch name.

After the automated build is finished you can pull the image:

```
docker pull registry.gitlab.com/pazdzewicz_docker/bitcoin-wallet-service:BRANCH
```

All images also have a master tag, just replace the BRANCH with "master". The "latest" Tag is only an alias for the "master" tag.

### Manual Build

You can also build the Docker image on your local pc:

```
docker build -t "mybitcoin-wallet-service:latest" .
```

### Build Options

Build Options are Arguments in the Dockerfile (`--build-arg`):

- None

### Security Check during Build

Before we release Docker Containers into the wild it is required to run the Anchore security checks over the Pipeline (this doesn't apply to manual built images). You can download the current artifacts on the Pipelines page (the download drop-down).

## Environment Variables:

## Ports

- 7000 - JSON RPC Server

## Usage

### docker-compose

```
docker-compose pull
docker-compose up -d
```

## Examples

### Get Current Version

```
curl --data-binary '{"jsonrpc":"2.0","id":"curltext","method":"version","params":[]}' http://${ELECTRUM_USER}:${ELECTRUM_PASSWORD}@127.0.0.1:7000
```

### List Addresses of Wallet

```
curl --data-binary '{"jsonrpc":"2.0","id":"curltext","method":"listaddresses","params":{"wallet":"/wallet/default_wallet"}}' http://${ELECTRUM_USER}:${ELECTRUM_PASSWORD}@127.0.0.1:7000
```

### Get Balance of Wallet

```
curl --data-binary '{"jsonrpc":"2.0","id":"curltext","method":"getbalance","params":{"wallet":"/wallet/default_wallet"}}' http://${ELECTRUM_USER}:${ELECTRUM_PASSWORD}@127.0.0.1:7000
```

More API Information: https://github.com/AraneaDev/laravel-electrum/blob/master/src/Electrum.php