#!/usr/bin/env bash
ELECTRUM="$(which electrum)"

# Network switch
if [ "${ELECTRUM_NETWORK}" = "mainnet" ]; then
  FLAGS=''
elif [ "${ELECTRUM_NETWORK}" = "testnet" ]; then
  FLAGS='--testnet'
elif [ "${ELECTRUM_NETWORK}" = "regtest" ]; then
  FLAGS='--regtest'
elif [ "${ELECTRUM_NETWORK}" = "simnet" ]; then
  FLAGS='--simnet'
fi

# Set config
echo "setconfig rpcuser ${ELECTRUM_USER}"
# shellcheck disable=SC2086
${ELECTRUM} --offline ${FLAGS} setconfig rpcuser ${ELECTRUM_USER}

echo "setconfig rpcpassword ${ELECTRUM_PASSWORD}"
# shellcheck disable=SC2086
${ELECTRUM} --offline ${FLAGS} setconfig rpcpassword ${ELECTRUM_PASSWORD}

echo "setconfig rpchost ${ELECTRUM_HOST}"
# shellcheck disable=SC2086
${ELECTRUM} --offline ${FLAGS} setconfig rpchost ${ELECTRUM_HOST}

echo "setconfig rpcport ${ELECTRUM_PORT}"
# shellcheck disable=SC2086
${ELECTRUM} --offline ${FLAGS} setconfig rpcport ${ELECTRUM_PORT}

# Set default currency
if [ -n "${ELECTRUM_FIAT_CURRENCY}" ]
then
  echo "setconfig currency ${ELECTRUM_FIAT_CURRENCY}"
  # shellcheck disable=SC2086
  ${ELECTRUM} --offline ${FLAGS} setconfig currency ${ELECTRUM_FIAT_CURRENCY}
fi


# Run application

echo "start electrum daemon with flags \"${FLAGS}\""
# shellcheck disable=SC2086
${ELECTRUM} ${FLAGS} daemon -d

echo "electrum getinfo"
# shellcheck disable=SC2086
${ELECTRUM} ${FLAGS} getinfo

# Load wallet
echo "electrum load wallet"
if [ -f "${ELECTRUM_WALLET}" ]
then
  echo "load_wallet ${ELECTRUM_WALLET}"
  # shellcheck disable=SC2086
  ${ELECTRUM} ${FLAGS} load_wallet -w "${ELECTRUM_WALLET}" --password "${ELECTRUM_WALLET_PASSWORD}"

  echo "listaddresses ${ELECTRUM_WALLET}"
  # shellcheck disable=SC2086
  ${ELECTRUM} ${FLAGS} listaddresses -w "${ELECTRUM_WALLET}"
else
  echo "create ${ELECTRUM_WALLET}"
  # shellcheck disable=SC2086
  ${ELECTRUM} ${FLAGS} create -w "${ELECTRUM_WALLET}" --password "${ELECTRUM_WALLET_PASSWORD}"
fi

echo "list_wallets"
# shellcheck disable=SC2086
${ELECTRUM} ${FLAGS} list_wallets

# Wait forever
while true; do
  tail -f /dev/null & wait ${!}
done