FROM python:3.9.12-buster

ARG ELECTRUM_VERSION
ENV ELECTRUM_VERSION=${ELECTRUM_VERSION}

ENV ELECTRUM_USER electrum
ENV ELECTRUM_PASSWORD passw0rd
ENV ELECTRUM_HOME /home/$ELECTRUM_USER
ENV ELECTRUM_NETWORK mainnet

RUN mkdir -p /data \
             /wallet \
             ${ELECTRUM_HOME} \
             ${ELECTRUM_HOME}/.electrum/wallets/mainnet/ \
             ${ELECTRUM_HOME}/.electrum/wallets/testnet/ \
             ${ELECTRUM_HOME}/.electrum/wallets/regtest/ \
             ${ELECTRUM_HOME}/.electrum/wallets/simnet/ && \
    ln -sf /data ${ELECTRUM_HOME}/.electrum 

# IMPORTANT: always verify gpg signature before changing a hash here!
ENV ELECTRUM_CHECKSUM_SHA512 $CHECKSUM_SHA512

COPY src/entrypoint.bash /usr/local/bin/

RUN chmod +x /usr/local/bin/entrypoint.bash && \
    adduser --home ${ELECTRUM_HOME} ${ELECTRUM_USER} && \
    chown -R ${ELECTRUM_USER} ${ELECTRUM_HOME}/.electrum \
                              /data \
                              /wallet && \
    apt update -y && \
    apt -y install libsecp256k1-dev \
                   jq && \
    wget https://download.electrum.org/${ELECTRUM_VERSION}/Electrum-${ELECTRUM_VERSION}.tar.gz && \
    pip3 install cryptography \
                 pycryptodomex \
                 Electrum-${ELECTRUM_VERSION}.tar.gz && \
    rm -f Electrum-${ELECTRUM_VERSION}.tar.gz

USER $ELECTRUM_USER
WORKDIR $ELECTRUM_HOME
VOLUME /data
EXPOSE 7000

ENTRYPOINT ["entrypoint.bash"]